## Import des packages requis :
library(groundhog)                             # favorise la reproductibilité
today <- "2021-10-15"                          # date de version des packages
groundhog.library("beeswarm", date = today)    # représentation graphique
groundhog.library("data.table", date = today)  # chargement rapide de gros fichiers
groundhog.library("fs", date = today)          # outils divers de manip de fichiers
groundhog.library("here", date = today)        # gestion des chemins dans un projet
groundhog.library("nbconvertR", date = today)  # export de notebooks .ipynb -> .R
groundhog.library("repr", date = today)        # pour les graphiques sous Jupyter
groundhog.library("RJSONIO", date = today)     # import de fichiers .json
## Quelques packages additionnels :
groundhog.library(
    pkg = "dplyr",
    date = today,
    ignore.deps = c("crayon", "lifecycle", "rlang", "pillar")
)
#groundhog.library(
#    pkg = "plotly",
#    date = today,
#    ignore.deps = c("crayon", "lifecycle", "rlang", "pillar")
#)
#groundhog.library("textshape", date = today)
#groundhog.library("tidyr", date = today)

## Un exemple d'utilisation de here :
here()

## Construire le chemin vers le sous-répertoire "data" :
here("data")

## Construire le chemin vers le sous-répertoire "data/sujets/" :
here("data", "sujets")

## Exportation du notebook en script R :
## nbconvert(
##    file = here("jour2", "01_workflow_jupyter.ipynb"),
##    fmt = "script" # format choisi pour l'export : script R
## )
